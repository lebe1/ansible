# Ansible How To Step by Step
1. Adjust your configurations in ansible.cfg
2. Open a terminal and for example type "sudo nano /etc/hosts"  
3. Now you need to type in the IP addresses and your desired DNS names as follows: 192.168.20.167 vm1.nodekite.com
4. Open the hosts file and change line 19 to your Server´s DNS Name like: vm1.nodekite.com 
5. Being in the right directory, you can now type in "ansible-playbook ansible/site.yml"
---
P.S. Starting off by scratch with Ansible this website helped me to understand the fundamentals:
https://linuxhint.com/begineers_guide_tutorial_ansible/
